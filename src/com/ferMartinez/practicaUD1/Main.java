package com.ferMartinez.practicaUD1;

import com.ferMartinez.practicaUD1.base.Producto;
import com.ferMartinez.practicaUD1.gui.ProductoControlador;
import com.ferMartinez.practicaUD1.gui.ProductoModelo;
import com.ferMartinez.practicaUD1.gui.Ventana;

public class Main {

    public static void main(String[] args) {
        Ventana vista = new Ventana();
        ProductoModelo modelo = new ProductoModelo();
        ProductoControlador controlador = new ProductoControlador(vista, modelo);
    }
}
