package com.ferMartinez.practicaUD1.base;

import java.time.LocalDate;

public class Revista  extends Producto{
    private int numPaginas;

    public Revista() {
        super();
    }

    public Revista(String serie, String autor, LocalDate fechaMatriculacion, String numventas, int numPag) {
        super(serie, autor, fechaMatriculacion, numventas);
        this.numPaginas=numPag;
    }


    public int getNumPaginas() {
        return numPaginas;
    }

    public void setNumPaginas(int numPaginas) {
        this.numPaginas = numPaginas;
    }

    @Override
    public String toString() {
        return "Revista: "+getSerializacion()+" "+getAutor()+" "
                +getN_ventas()+" "+getNumPaginas();
    }
}
