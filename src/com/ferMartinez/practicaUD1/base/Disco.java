package com.ferMartinez.practicaUD1.base;

import java.time.LocalDate;

public class Disco extends Producto{

    private int numCanciones;

    public Disco() {
        super();
    }

    public Disco(String serie, String autor, LocalDate fechaMatriculacion, String numventas, int numCan) {
        super(serie, autor, fechaMatriculacion, numventas);
        this.numCanciones=numCan;
    }


    public int getNumCanciones() {
        return numCanciones;
    }

    public void setNumCanciones(int numCanciones) {
        this.numCanciones = numCanciones;
    }

    @Override
    public String toString() {
        return "Disco: "+getSerializacion()+" "+getAutor()+" "
                +getN_ventas()+" "+getNumCanciones();
    }
}
