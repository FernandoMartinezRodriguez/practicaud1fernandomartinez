package com.ferMartinez.practicaUD1.base;

import java.time.LocalDate;

public class Producto {

    private String serializacion;
    private String autor;
    private LocalDate fechaPublicacion;
    private String n_ventas;

    public Producto(String serializacion, String autor, LocalDate fechaPublicacion, String n_ventas) {
        this.serializacion = serializacion;
        this.autor = autor;
        this.fechaPublicacion = fechaPublicacion;
        this.n_ventas = n_ventas;
    }

    public Producto(){}

    public String getSerializacion() {
        return serializacion;
    }

    public void setSerializacion(String serializacion) {
        this.serializacion = serializacion;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public LocalDate getFechaPublicacion() {
        return fechaPublicacion;
    }

    public void setFechaPublicacion(LocalDate fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public String getN_ventas() {
        return n_ventas;
    }

    public void setN_ventas(String n_ventas) {
        this.n_ventas = n_ventas;
    }
}
