package com.ferMartinez.practicaUD1.gui;

import com.ferMartinez.practicaUD1.base.Producto;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Ventana {
    JRadioButton revistaRadioButton;
    private JPanel panel1;
    JRadioButton discoRadioButton;
    JTextField serieTxt;
    JTextField autorTxt;
    JTextField ventasTxt;
    JTextField paginasTxt;
    JList list1;
    JButton btnImportar;
    JButton btnNuevo;
    JButton btnExportar;
    DatePicker date;
    JLabel paginasLbl;
    private JLabel serielbl;
    private JLabel autolbl;
    private JLabel fechalbl;
    private JLabel ventaslbl;
    private JButton btnListar;
    public JFrame frame;

    public DefaultListModel<Producto> dlmProducto;

    public Ventana() {
        frame = new JFrame("Vehiculos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmProducto=new DefaultListModel<Producto>();
        list1.setModel(dlmProducto);
    }
}
