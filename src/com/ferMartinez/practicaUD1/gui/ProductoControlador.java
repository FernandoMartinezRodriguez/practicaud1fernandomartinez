package com.ferMartinez.practicaUD1.gui;

import com.ferMartinez.practicaUD1.base.Producto;
import com.ferMartinez.practicaUD1.util.Util;
import org.xml.sax.SAXException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;

public class ProductoControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private ProductoModelo modelo;
    private File ultimaRutaExportada;

    public ProductoControlador(Ventana vista, ProductoModelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Serialización\nAutor\nVentas\nFecha" +
                            vista.paginasLbl.getText());
                    break;
                }

                if (modelo.existeSerie(vista.serieTxt.getText())) {
                    Util.mensajeError("Ya existe un producto con esa serie\n+" +
                            vista.serieTxt.getText());
                    break;
                }
                if (vista.revistaRadioButton.isSelected()) {
                    modelo.altaRevista(vista.serieTxt.getText(), vista.autorTxt.getText(), vista.ventasTxt.getText(),
                            vista.date.getDate(), Integer.parseInt(vista.paginasTxt.getText()));
                } else {
                    modelo.altaDisco(vista.serieTxt.getText(), vista.autorTxt.getText(), vista.ventasTxt.getText(),
                            vista.date.getDate(), Integer.parseInt(vista.paginasTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Revista":
                vista.paginasLbl.setText("Nº de páginas");
                break;
            case "Disco":
                vista.paginasLbl.setText("Nº de canciones");
                break;
            case "Listar":
                modelo.Lis();
                break;
    }

    }



    private boolean hayCamposVacios() {
        if (vista.paginasTxt.getText().isEmpty() ||
                vista.serieTxt.getText().isEmpty() ||
                vista.autorTxt.getText().isEmpty() ||
                vista.ventasTxt.getText().isEmpty() ||
                vista.date.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void limpiarCampos() {
        vista.paginasTxt.setText(null);
        vista.serieTxt.setText(null);
        vista.autorTxt.setText(null);
        vista.ventasTxt.setText(null);
        vista.date.setText(null);
        vista.autorTxt.requestFocus();
    }

    private void refrescar() {
        vista.dlmProducto.clear();
        for (Producto unProducto : modelo.obtenerProductos()) {
            vista.dlmProducto.addElement(unProducto);
        }
    }

    private void addActionListener(ActionListener listener) {
        vista.revistaRadioButton.addActionListener(listener);
        vista.discoRadioButton.addActionListener(listener);
        vista.btnExportar.addActionListener(listener);
        vista.btnImportar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener) {

        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.list1.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("productos.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("productos.conf"), "Datos configuracion vehiculos");

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}
