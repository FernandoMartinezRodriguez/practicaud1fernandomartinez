package com.ferMartinez.practicaUD1.gui;

import com.ferMartinez.practicaUD1.base.Disco;
import com.ferMartinez.practicaUD1.base.Producto;
import com.ferMartinez.practicaUD1.base.Revista;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ProductoModelo {



    private ArrayList<Producto> listaProductos;

    public ProductoModelo() {
        listaProductos = new ArrayList<Producto>();
    }

    public ArrayList<Producto> obtenerProductos() {
        return listaProductos;
    }

    public void altaRevista(String serie, String autor, String ventas, LocalDate fechaMatriculacion, int numPaginas) {
        Revista nuevaRevista=new Revista(serie,autor, fechaMatriculacion,ventas,numPaginas);
        listaProductos.add(nuevaRevista);
    }

    public void altaDisco(String serie, String autor, String ventas, LocalDate fechaMatriculacion, int numCanciones) {
        Disco nuevoDisco= new Disco(serie,autor,fechaMatriculacion,ventas,numCanciones);
        listaProductos.add(nuevoDisco);
    }

    public boolean existeSerie(String serie) {
        for (Producto unProducto:listaProductos) {
            if(unProducto.getSerializacion().equals(serie)) {
                return true;
            }
        }
        return false;
    }

    public void lisss(String ventas) {
        for (Producto unProducto:listaProductos) {
            if(unProducto instanceof Revista){
                unProducto=listaProductos[1];
            }
        }
    }


    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Productos");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoProducto = null, nodoDatos = null;
        Text texto = null;

        for (Producto unProducto : listaProductos) {

            /*Añado dentro de la etiqueta raiz (Vehiculos) una etiqueta
            dependiendo del tipo de vehiculo que este almacenando
            (coche o moto)
             */
            if (unProducto instanceof Revista) {
                nodoProducto = documento.createElement("Revista");

            } else {
                nodoProducto = documento.createElement("Disco");
            }
            raiz.appendChild(nodoProducto);

            /*Dentro de la etiqueta vehiculo le añado
            las subetiquetas con los datos de sus
            atributos (matricula, marca, etc)
             */
            nodoDatos = documento.createElement("serializacion");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getSerializacion());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("autor");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getAutor());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("ventas");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getN_ventas());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-matriculacion");
            nodoProducto.appendChild(nodoDatos);

            texto = documento.createTextNode(unProducto.getFechaPublicacion().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de vehiculo
            debo acceder a él controlando el tipo de objeto
             */
            if (unProducto instanceof Revista) {
                nodoDatos = documento.createElement("numero-paginas");
                nodoProducto.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Revista) unProducto).getNumPaginas()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("numero-canciones");
                nodoProducto.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Disco) unProducto).getNumCanciones()));
                nodoDatos.appendChild(texto);
            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaProductos = new ArrayList<Producto>();
        Revista nuevaRevista = null;
        Disco nuevoDisco = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoVehiculo = (Element) listaElementos.item(i);


            if (nodoVehiculo.getTagName().equals("Revista")) {
                nuevaRevista = new Revista();
                nuevaRevista.setSerializacion(nodoVehiculo.getChildNodes().item(0).getTextContent());
                nuevaRevista.setAutor(nodoVehiculo.getChildNodes().item(1).getTextContent());
                nuevaRevista.setN_ventas(nodoVehiculo.getChildNodes().item(2).getTextContent());
                nuevaRevista.setFechaPublicacion(LocalDate.parse(nodoVehiculo.getChildNodes().item(3).getTextContent()));
                nuevaRevista.setNumPaginas(Integer.parseInt(nodoVehiculo.getChildNodes().item(4).getTextContent()));

                listaProductos.add(nuevaRevista);
            } else {
                if (nodoVehiculo.getTagName().equals("Disco")) {
                    nuevoDisco = new Disco();
                    nuevoDisco.setSerializacion(nodoVehiculo.getChildNodes().item(0).getTextContent());
                    nuevoDisco.setAutor(nodoVehiculo.getChildNodes().item(1).getTextContent());
                    nuevoDisco.setN_ventas(nodoVehiculo.getChildNodes().item(2).getTextContent());
                    nuevoDisco.setFechaPublicacion(LocalDate.parse(nodoVehiculo.getChildNodes().item(3).getTextContent()));
                    nuevoDisco.setNumCanciones(Integer.parseInt(nodoVehiculo.getChildNodes().item(4).getTextContent()));

                    listaProductos.add(nuevoDisco);
                }
            }


        }
    }
}
